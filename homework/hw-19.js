"use strict"

// Упражнение 1

let a = `$100`
let b = `300$`

// Решение 

let summ = (Number(a.slice(1)) + (Number(parseFloat(b))));

console.log(summ);// Должно быть 400

// Упражнение 2

let message = ` привет, медвед      `;

// Решение 

message = message.trim()

message = message[0].toUpperCase() + message.slice(1)

console.log(message);// “Привет, медвед”

// Упражнение 3

//0 - 3 года — младенец
//4 - 11 лет — ребенок
//12 - 18 — подросток
//19 - 40 — познаёте жизнь
//41 - 80 — познали жизнь
//81 и больше — долгожитель


// Решение

let age = Number(prompt("Сколько Вам лет?"));

if (age < 3) {
    console.log(`Вам ${age} лет и вы младенец`);

} else if (age < 11) {
    console.log(`Вам ${age} лет и вы ребенок`);

} else if (age < 18) {
    console.log(`Вам ${age} лет и вы подросток`);

} else if (age < 40) {
    console.log(`Вам ${age} лет и вы познаёте жизнь`);

} else if (age < 80) {
    console.log(`Вам ${age} лет и вы познали жизнь`);

} else if (age > 81) {
    console.log(`Вам ${age} лет и вы долгожитель`);
} else {
    console.log(`Непрвильно указан возраст`)
}
// Упражнение 4

let message2 = 'Я работаю со строками как профессионал!'; 

// Решение

let count = message2.split(' ').length;// Ваше решение

console.log(count);// Должно быть 6
