"use strict"

import { product, review1, review2 } from "../new/data.js";
console.log(product);

//Лекция

/* let user = {
    name: `John`,
    age: 30,
    isAdmin: true,
}

for (let key in user) {
    console.log(key);

    console.log(user[key]);
} */


/* let user = {};

console.log(user.noSuchProperty === undefined);
let product = { price: 2999 };

console.log(`price` in product);

console.log(`blabla` in user); */

// true

/* Упражнение 1 */

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

let car = {};

console.log(isEmpty(car)); /* true */

car.age = 12;

console.log(isEmpty(car)); /* false */

/* Упражнение 2 */

/* Вы данных с другого файла */

for (let key in product) {
    console.log(key);
    console.log(product[key]);
}

for (let key in review1) {
    console.log(key);
    console.log(review1[key]);
}

for (let key in review2) {
    console.log(key);
    console.log(review2[key]);
}





/* Упражнение 3 */

/* Повышение Зарплаты на опредененный %  */
const raiseSalary = (prop, percent) => {
    let raise = 0

    for (const key in prop) {
        prop[key] *= 1 + percent / 100;
        raise += prop[key]
    }

    console.log(`Общие затраты`, raise);

    return prop;
};

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

console.log(`Зарплата сейчас`, salaries);

const result = raiseSalary(salaries, 5);

console.log(`Зарплата после повышения`, result);


/* let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000
  };
  
  function raiseSalary(perzent) {
    let newSalaries = {};
  
    for (let key in salaries) {
      let raise = (salaries[key] * perzent) / 100;
  
      newSalaries[key] = salaries[key] + raise;
    }
  
    return newSalaries;
  }
  
  function calcSumm(obj) {
    let summ = 0;
  
    for (let key in obj) {
      summ = summ + obj[key];
    }
  
    return summ;
  }
  
  let newSalaries = raiseSalary(10);
  let beforeSumm = calcSumm(salaries);
  let summ = calcSumm(newSalaries);
  
  console.log(summ - beforeSumm); */
  