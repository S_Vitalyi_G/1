"use strict"

// Упражнение 1

let a = '100px';
let b = '323px';

// Решение

let result = (parseFloat(a)) + (parseFloat(b));

console.log(result);

//Упражнение 2

console.log(Math.max(10, -45, 102, 36, 12, 0, -1.));

// Упражниене 3

let c = 123.3399;

// Решение 

console.log(Math.round(c));

// Упражниене 4

let d = 0.111;

//Решение

console.log(Math.ceil(d));

//Упражнение 5

let e = 45.333333;

//Решение

console.log(e.toFixed(1));

// Упражнение 6

let f = 3

// Решение

console.log(3 ** 5);

// Упражниене 7

let g=400000000000000;

// Решение 

console.log(4e14)

// Упражние 8

let h='1'!=1;

// Решение 

console.log('1'!==1);

// Упражние 9

console.log(0.1+0.2===0.3);

// Решение 

// это совсем разные числа по этому при сравнении не могут быть равны
