"use strict"

/* Упражнение 1 */

for (let a = 2; a <= 20; a = a + 2) {
    console.log(a);
} 

/* Упражниние 2 */

 let sum = 0;
let i = 0;
 
while (i < 3) {
    let value = +prompt("Введите число", "");
 
    if (!value) {
        alert("Вы ввели не число!");
        break;
    }
    sum += value;
    i++
}
console.log("Сумма: " + sum);

/* Упражнение 3 */

 const NameOfMonth = [`Январь`, `Февраль`, `Март`, `Апрель`, `Май`, `Июнь`, `Июль`, `Августь`, `Сентябрь`, `Октябрь`, `Ноябрь`, `Декабрь`]
const getNameOfMonth = (numberMonth) => {
    return NameOfMonth[numberMonth];
 
}
 
console.log(getNameOfMonth(4))
 
for (let a = 0; a < NameOfMonth.length; a++) {
    if (NameOfMonth[a] !== 'Октябрь') console.log(getNameOfMonth(a));
}


/*   !function () {

      let message = "Hello";

      alert(message); // Hello

  }();

  Immediately Invoked Function Expression, IIFE — это функция, которая выполняется сразу же после того, 
  как была определена. */
  /* Используйте IIFE для изолированных модулей или для эмуляции top-level await */